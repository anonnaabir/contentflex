        <?php
        if ( !defined( 'ABSPATH' ) ) { exit; }
        if ( ! isset( $content_width ) ) $content_width = 1280;

        if ( ! defined( 'DYNOSHOP_VERSION' ) ) {
            define( 'DYNOSHOP_VERSION', '1.0' );
        }

        // Required Functions Start

        require get_template_directory() . '/inc/enqueue.php';   // Script Enqueue Functions
        require get_template_directory() . '/inc/theme-setup.php';  // All Theme Setup Functions
        require get_template_directory() . '/inc/admin.php';  // Admin Panel by CSF
        require get_template_directory() . '/inc/demo-import.php';


        // Auto Updater Function

        require 'lib/updater/plugin-update-checker.php';
        $WPDynoshopUpdater = Puc_v4_Factory::buildUpdateChecker(
        'https://api.themespell.com/updater/?action=get_metadata&slug=contentflex',
        __FILE__,
        'contentflex'
        );


        function contentflex_register_elementor_locations( $elementor_theme_manager ) {
            $elementor_theme_manager->register_all_core_location();
        };
        add_action( 'elementor/theme/register_locations', 'contentflex_register_elementor_locations' );


