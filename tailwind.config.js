module.exports = {
  purge:{
    content: [
			'./src/**/*.php',
			'./template-parts/**/*.php',
			'./*.php',
			'./inc/**/*.php',
			'./inc/*.php',
      './lib/**/*.php',
			'./src/**/*.js',
		],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
