<?php


require_once get_template_directory() . '/lib/plugin-manager/class-tgm-plugin-activation.php';

function contentflex_register_required_plugins() {

	$plugins = array(
		array(
			'name'      => 'WooCommerce',
			'slug'      => 'woocommerce',
			'required'  => true,
		),
		array(
			'name'      => 'Elementor Page Builder',
			'slug'      => 'elementor',
			'required'  => true,
		),
		array(
			'name'      => 'Header & Footer Builder',
			'slug'      => 'header-footer-elementor',
			'required'  => true,
		),
		array(
			'name'      => 'Rig Elements',
			'slug'      => 'rig-elements',
			'source'    => 'https://gitlab.com/anonnaabir/rig-elements/-/archive/1.0-RC7/rig-elements-1.0-RC7.zip',
			'required'  => true,
			'external_url' => 'https://codember.com',
		),
		array(
			'name'      => 'Dynoshop Demo Import',
			'slug'      => 'one-click-demo-import',
			'required'  => true,
		),
	);

	$config = array(
		'id'           => 'contentflex',          // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'dynoshop-install-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => false,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => true,                   // Automatically activate plugins after installation or not.
		'message'      => '',
		'strings'     => array(
			'page_title'                      => __( 'Install Dynoshop Plugins', 'contentflex' ),
			'menu_title'                      => __( 'Install Plugins', 'contentflex' ),
			'notice_can_install_required'     => _n_noop(
				'Dynoshop requires the following plugin: %1$s.',
				'Dynoshop requires the following plugins: %1$s.',
				'contentflex'
			),
		),
	);

	tgmpa( $plugins, $config);
}


add_action( 'tgmpa_register', 'contentflex_register_required_plugins' );