<?php

			// Demo Import Function Start

			function contentflex_demo_import() {
				return array(
					array(
						'import_file_name'           => 'Camera Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2021/10/camerashop.WordPress.2021-10-27.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_camerashop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/camera-shop/',
					),

					array(
						'import_file_name'           => 'Furniture Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2021/10/furnitureshop.WordPress.2021-10-27.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_furnitureshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/furniture-shop/',
					),


					array(
						'import_file_name'           => 'Outdoor Gear Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2021/10/outdoorgear.WordPress.2021-10-27.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_outdoorgear-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/outdoor-gear/',
					),


					array(
						'import_file_name'           => 'Beard Care Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2021/10/beardcare.WordPress.2021-10-31.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_beardcare-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/beard-care/',
					),


					array(
						'import_file_name'           => 'Organic Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2021/10/organicshop.WordPress.2021-10-27.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_organicshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/organic-shop/',
					),


					array(
						'import_file_name'           => 'Bicycle Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2021/10/bicycleshop.WordPress.2021-10-27.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_bicycleshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/bicycle-shop/',
					),


					array(
						'import_file_name'           => 'Technology Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2021/10/technologyshop.WordPress.2021-10-27.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_techshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/tech-shop/',
					),


					array(
						'import_file_name'           => 'Flower Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2021/10/flowershop.WordPress.2021-10-27.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_flowershop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/flower-shop/',
					),


					array(
						'import_file_name'           => 'Footwear Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2021/10/footwear.WordPress.2021-10-27.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_footwaer-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/foot-wear/',
					),


					array(
						'import_file_name'           => 'Home Decoration Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2021/10/homedecoration.WordPress.2021-10-27.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_homedecoration-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/home-decoration/',
					),


					array(
						'import_file_name'           => 'GYM Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2021/10/gymshop.WordPress.2021-10-27.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_gymshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/gym-shop/',
					),


					array(
						'import_file_name'           => 'Perfume Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2021/10/perfumeshop.WordPress.2021-10-27.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_perfumeshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/perfume-shop/',
					),


					array(
						'import_file_name'           => 'Gift Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2021/10/gift-shop.WordPress.2021-10-27.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_giftshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/gift-shop/',
					),



					array(
						'import_file_name'           => 'Cake Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2021/10/dynoshopcakeshop.WordPress.2021-10-27.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_cakeshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/dynoshop-cake-shop/',
					),


					array(
						'import_file_name'           => 'Eyewear Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2021/10/eyewear.WordPress.2021-10-27.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_eyewearshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/eye-wear/',
					),


					array(
						'import_file_name'           => 'Video Game Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2021/10/videogameshop.WordPress.2021-10-31.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_videogameshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => '',
					),


					array(
						'import_file_name'           => 'Beauty Care Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2021/10/beautyshop.WordPress.2021-10-31.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_beautyshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/beauty-shop/',
					),


					array(
						'import_file_name'           => 'Pet Products Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2021/10/petshop.WordPress.2021-10-31.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_petshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/pet-shop/',
					),


					array(
						'import_file_name'           => 'Boutique Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2021/10/boutiqueshop.WordPress.2021-10-31.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_boutiqueshop-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/pet-shop/',
					),


					array(
						'import_file_name'           => 'Home Appliance Shop',
						// 'categories'                 => array( 'Category 1', 'Category 2' ),
						'import_file_url'            => 'https://dynoshop.themespell.com/wp-content/uploads/2021/10/homeappliance.WordPress.2021-10-31.xml',
						'import_preview_image_url'   => 'https://themespell.com/wp-content/uploads/2021/10/dynoshop_homeappliance-768x576.jpeg',
						'import_notice'              => __( 'After import the demo, just change your website homepage from WordPress Settings.', 'wp-localpress' ),
						'preview_url'                => 'https://dynoshop.themespell.com/pet-shop/',
					),
					
					
				);
			}
			add_filter( 'pt-ocdi/import_files', 'contentflex_demo_import' );


		
			function contentflex_demo_import_page_setup( $default_settings ) {
				$lp_options = get_option( 'dynoshop_options' );
				
				if (isset($lp_options['white_label_theme_name'])){
					$theme_name = $lp_options['white_label_theme_name'];
				}
				else {
					$theme_name = 'Dynoshop';
				}

				$default_settings['parent_slug'] = 'themes.php';
				$default_settings['page_title']  = esc_html__( $theme_name.' Demo Import' , 'contentflex' );
				$default_settings['menu_title']  = esc_html__( 'Import '.$theme_name.' Demos' , 'contentflex' );
				$default_settings['capability']  = 'import';
				// $default_settings['menu_slug']   = 'pt-one-click-demo-import';
			
				return $default_settings;
			}
			add_filter( 'pt-ocdi/plugin_page_setup', 'contentflex_demo_import_page_setup' );
		
		
			function contentflex_demo_import_page_title ($plugin_title ) {
				?>
				<h1 class="ocdi__title  dashicons-before  dashicons-upload"><?php $plugin_title  = esc_html_e( 'Dynoshop Demo Import', 'contentflex' ); ?></h1>
				<?php
			}
			add_filter( 'pt-ocdi/plugin_page_title', 'contentflex_demo_import_page_title' );
		
			add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );

			add_filter( 'pt-ocdi/import_memory_limit', '256M' );
		
		
		
			
		
			// Demo Import Function End






