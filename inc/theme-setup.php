<?php
				
            function contentflex_init() {

                require get_template_directory() . '/inc/plugin-setup.php';
                require get_template_directory() . '/inc/theme-settings/white-label.php';

                add_theme_support('custom-logo');
                add_theme_support( 'title-tag' );
                add_theme_support( 'post-thumbnails' );
                add_theme_support( 'automatic-feed-links' );
                add_theme_support( 'title-tag' );
                add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
                add_theme_support( 'custom-logo', array(
                    'width' => 260,
                    'height' => 100,
                    'flex-height' => true,
                    'flex-width' => true,
                ) );
                add_theme_support( 'custom-header' );
                add_theme_support( 'woocommerce' );
                add_post_type_support( 'page', 'excerpt' );

                register_nav_menus(
                    array( 'main-menu' => __( 'Main Menu', 'contentflex' ) )
                );

                load_theme_textdomain( 'contentflex', get_template_directory() . '/languages' );
            }


            add_action( 'after_setup_theme', 'contentflex_init' );
            


            // Added Custom Favicon Support

			function contentflex_add_favicon() {
				$lp_options = get_option( 'dynoshop_options' );
				if (isset($lp_options['contentflex_custom_favicon']['url'])) {
					$favicon = $lp_options['contentflex_custom_favicon']['url'];
					echo'<link rel="icon" href="'.$favicon.'" type="image/png" sizes="16x16">';
				}
			}
				
			add_action('wp_head', 'contentflex_add_favicon');



            function contentflex_comment_reply() {
                if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
            }
            add_action( 'comment_form_before', 'contentflex_comment_reply' );
