<?php

require_once get_theme_file_path() .'/lib/csf/codestar-framework.php';


        if( class_exists( 'CSF' ) ) {

            $prefix = 'dynoshop_options';
            $translatable_name = __('Dynoshop','contentflex');
            $lp_options = get_option( 'dynoshop_options' );
            $menu_display = $lp_options['limit_access_hide_dynoshop_settings'];

            CSF::createOptions( $prefix, array(
            'framework_title'  => $translatable_name.' Options',
            'framework_class'  => '',
            'menu_title' => $translatable_name.' Options',
            'menu_slug'  => 'dynoshop-options',
            'menu_icon'  => 'dashicons-admin-home',
            'menu_hidden' => $menu_display,
            'show_bar_menu' => false,
            // 'show_in_customizer'  => true,
            'footer_credit'  => 'Powered By <a href="https://themespell.com/local-business-wordpress-theme/">Dynoshop</a>. Made with ♥ by Team <a href="https://themespell.com">Themespell</a>'
            ) );
        
            
            CSF::createSection( $prefix, array(
            'title'  => 'General Settings',
            'fields' => array(

              array(
                'type'    => 'heading',
                'content' => 'General Settings',
              ),

              
              array(
                'id'    => 'contentflex_custom_favicon',
                'type'  => 'media',
                'title' => 'Custom Favicon',
                'library' => 'image',
                'preview_width' => 64,
                'preview_height' => 64
              ),
              
            )
            ) );


            CSF::createSection( $prefix, array(
                'title'  => 'White Labeling',                
                'fields' => array(

                  array(
                    'type'    => 'heading',
                    'content' => 'White Labeling',
                  ),

                  array(
                    'id'    => 'contentflex_white_label',
                    'type'  => 'switcher',
                    'title' => 'Enable White Labeling',
                  ),

                  array(
                    'type'    => 'subheading',
                    'content' => 'Theme Settings',
                    'dependency' => array( 'contentflex_white_label', '==', 'true' ),
                ),

                array(
                  'id'      => 'white_label_theme_name',
                  'type'    => 'text',
                  'title'   => 'Theme Name',
                  'dependency' => array( 'contentflex_white_label', '==', 'true' ),
                ),

                array(
                  'id'      => 'white_label_theme_description',
                  'type'    => 'textarea',
                  'title'   => 'Theme Description',
                  'dependency' => array( 'contentflex_white_label', '==', 'true' ),
                ),

                array(
                  'id'      => 'white_label_theme_version',
                  'type'    => 'text',
                  'title'   => 'Theme Version',
                  'dependency' => array( 'contentflex_white_label', '==', 'true' ),
                ),

                array(
                  'id'      => 'white_label_theme_author',
                  'type'    => 'text',
                  'title'   => 'Theme Author',
                  'dependency' => array( 'contentflex_white_label', '==', 'true' ),
                ),

                array(
                  'id'      => 'white_label_theme_author_url',
                  'type'    => 'text',
                  'title'   => 'Author URL',
                  'dependency' => array( 'contentflex_white_label', '==', 'true' ),
                ),

                array(
                  'id'      => 'white_label_theme_screenshot',
                  'type'    => 'media',
                  'title'   => 'Theme Screenshot',
                  'library' => 'image',
                  'dependency' => array( 'contentflex_white_label', '==', 'true' ),
                ),


                array(
                  'id'      => 'white_label_theme_tags',
                  'type'    => 'text',
                  'title'   => 'Theme Tags',
                  'dependency' => array( 'contentflex_white_label', '==', 'true' ),
                ),


                array(
                  'type'    => 'subheading',
                  'content' => 'Plugin Settings',
                  'dependency' => array( 'contentflex_white_label', '==', 'true' ),
              ),
                
              
              array(
                'id'    => 'white_label_hide_plugins',
                'type'  => 'switcher',
                'title' => 'Hide Recommended Plugins',
                'default' => true,
                'dependency' => array( 'contentflex_white_label', '==', 'true' ),
              ),

              array(
                'id'    => 'white_label_hide_demo_import',
                'type'  => 'switcher',
                'title' => 'Hide Demo Import',
                'default' => true,
                'dependency' => array( 'contentflex_white_label', '==', 'true' ),
              ),

                )
                ) );

                CSF::createSection( $prefix, array(
                  'title'  => 'Limit Access',
                  'fields' => array(

                    array(
                      'type'    => 'heading',
                      'content' => 'Limit Access',
                    ),

                    // A Notice
                  array(
                    'type'    => 'notice',
                    'style'   => 'warning',
                    'content' => 'Please save this URL first. As you can access the settings directly: <strong>'.get_site_url().'/wp-admin/admin.php?page=dynoshop-options</strong>',
                  ),


                    array(
                      'id'    => 'limit_access_hide_dynoshop_settings',
                      'type'  => 'switcher',
                      'title' => 'Hide Dynoshop Settings',
                    ),

                    array(
                      'id'    => 'limit_access_hide_elementor_settings',
                      'type'  => 'switcher',
                      'title' => 'Hide Elementor Settings',
                    ),

                    array(
                      'id'    => 'limit_access_hide_wordpress_settings',
                      'type'  => 'switcher',
                      'title' => 'Hide WordPress Settings',
                    ),

                    array(
                      'id'    => 'limit_access_hide_theme_settings',
                      'type'  => 'switcher',
                      'title' => 'Hide Theme Settings',
                    ),

                    array(
                      'id'    => 'limit_access_hide_plugin_settings',
                      'type'  => 'switcher',
                      'title' => 'Hide Plugin Settings',
                    ),

                    array(
                      'id'    => 'limit_access_hide_user_settings',
                      'type'  => 'switcher',
                      'title' => 'Hide User Settings',
                    ),

                    array(
                      'id'    => 'limit_access_hide_tools_settings',
                      'type'  => 'switcher',
                      'title' => 'Hide Tools Settings',
                    ),

                  )
                ) );
        
        }
  