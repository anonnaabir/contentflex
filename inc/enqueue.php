<?php

    	// Enqueue scripts and styles.

		function contentflex_scripts() {
			$theme = wp_get_theme();
			$scripts_version = $theme->get('Version');
			
			wp_enqueue_style('contentflex', get_template_directory_uri() . '/css/theme.css', array(), $scripts_version);
			wp_enqueue_script('contentflex', get_template_directory_uri() . '/node_modules/bootstrap/dist/js/bootstrap.min.js', array(), true, $scripts_version);
			}
            
			add_action( 'wp_enqueue_scripts', 'contentflex_scripts', 100);